const express = require('express');
const bodyparser = require('body-parser');
const misRutas = require('./router/index');
const path = require('path');

const app = express();

app.use(express.static(__dirname + "/Public"));
app.use(bodyparser.urlencoded({extended:true}));
app.set('view engine', 'ejs');
app.use(misRutas)
app.engine('html', require('ejs').renderFile)


const puerto = 3014;

app.listen(puerto,()=>{
    console.log("iniciando puerto 3014")
})