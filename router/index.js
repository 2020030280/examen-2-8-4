const express = require('express');
const bodyparser = require('body-parser');
const router = express.Router();

router.get('/', (req,res)=>{

    const valores={
        Num: req.query.Num,
        Nom: req.query.Nom,
        DOM: req.query.DOM,
        tipo: req.query.tipo,
        KC: req.query.KC
    }

    res.render('comluz.html', valores)
})

router.post('/', (req,res)=>{

    const valores={
        Num: req.body.Num,
        Nom: req.body.Nom,
        DOM: req.body.DOM,
        tipo: req.body.tipo,
        KC: req.body.KC,
        sub: req.body.sub,
        imp: req.body.imp,
        desc: req.body.desc
    }

    res.render('comluz.html', valores)
})
router.get('/resultados', (req,res)=>{

    const valores={
        Num: req.query.Num,
        Nom: req.query.Nom,
        DOM: req.query.DOM,
        tipo: req.query.tipo,
        KC: req.query.KC,
        sub: req.query.sub,
        desc: req.query.desc,
        imp: req.query.imp,
    }

    res.render('comluz2.html', valores)
})
router.post('/resultados', (req,res)=>{

    const valores={
        Num: req.body.Num,
        Nom: req.body.Nom,
        DOM: req.body.DOM,
        tipo: req.body.tipo,
        KC: req.body.KC,
        sub: req.body.sub,
        imp: req.body.imp,
        desc: req.body.desc

    }
    res.render('comluz2.html', valores)
})
router.use((req,res,next)=>{

        res.status(404).sendFile(__dirname + '/Public/error.html')
    
    })
module.exports=router;